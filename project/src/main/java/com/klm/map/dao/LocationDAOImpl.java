package com.klm.map.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.klm.map.model.Location;

/**
 * Provides the DAO to get locations data.
 * @author Kelum
 *
 */
@Repository
public class LocationDAOImpl implements LocationDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<Location> getLocations() {
		return sessionFactory.getCurrentSession().createQuery("from Location")
				.list();
	}

}
