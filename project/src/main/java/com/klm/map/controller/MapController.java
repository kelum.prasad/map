package com.klm.map.controller;

import java.io.IOException;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.klm.map.model.Location;
import com.klm.map.service.LocationService;

/**
 * Request Controller class- Facilitate to return locations.jsp with map location data.
 * @author Kelum
 *
 */
@Controller
public class MapController {

	private static final Logger logger = Logger
			.getLogger(MapController.class);

	public MapController() {
		System.out.println("MapController()");
	}

	@Autowired
	private LocationService locationService;
	
	@RequestMapping(value = "/")
	public ModelAndView listLocations(ModelAndView model) throws IOException {
		List<Location> listLocations = locationService.getLocations();
		model.addObject("locations", listLocations);
		model.setViewName("locations");
		return model;
	}
	
}
