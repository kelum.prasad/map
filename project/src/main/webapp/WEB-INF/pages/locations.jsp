<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
	<style>
        #map-canvas {
         width: 100%;
    		height: 600px;
      }
    </style>
    
    
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=visualization,geometry"></script>

	<script type="text/javascript">
	
		//build locations list.
		var locations = [
			
			<c:forEach varStatus='status' items='${locations}' var ="location">
		        {
		            "lat":${location.latitude},
		            "lng":${location.longitude},
			        "desc":"${location.description}"
		        }
		        <c:if test="${!status.last}">,</c:if>
	    	</c:forEach>
	    
		]

		function initialize() {
			var i;
			var mapOptions = {
	                zoom: 10,
	                center: new google.maps.LatLng(7,80),
	                mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
				
			var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
				
			var infowindow =  new google.maps.InfoWindow();
			
			
			if ( locations.length > 0 ) {
				for (i = 0; i < locations.length; i++) {
					// create a marker
					var marker = new google.maps.Marker({
						icon:"/map/resources/location.png",
						position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
						map: map
					});
					
					
					// add an event listener for this marker
					bindInfoWindow(marker, map, infowindow, "<p>" + locations[i].desc + "</p>");  
				}
			}
			
		}

		function bindInfoWindow(marker, map, infowindow, html) { 
			google.maps.event.addListener(marker, 'mouseover', function() { 
				infowindow.setContent(html); 
				infowindow.open(map, marker); 
			}); 
			google.maps.event.addListener(marker, 'mouseout', function() { 
				infowindow.close(); 
			}); 
			
		} 

	    google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</head>
<body>
<div id="map-canvas"></div>

</body>
</html>
