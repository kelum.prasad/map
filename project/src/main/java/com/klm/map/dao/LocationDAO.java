package com.klm.map.dao;

import java.util.List;

import com.klm.map.model.Location;

/**
 * 
 * @author Kelum
 *
 */
public interface LocationDAO {

	public List<Location> getLocations();
}
