package com.klm.map.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.klm.map.dao.LocationDAO;
import com.klm.map.model.Location;

/**
 * Provides the business logic for get locations data.
 * @author Kelum
 *
 */
@Service
@Transactional
public class LocationServiceImpl  implements LocationService {

	@Autowired
	private LocationDAO locationDAO;

	@Transactional
	public List<Location> getLocations() {
		return locationDAO.getLocations();
	}


}
