package com.klm.map.service;

import java.util.List;

import com.klm.map.model.Location;

/**
 * 
 * @author Kelum
 *
 */
public interface LocationService {

	public List<Location> getLocations();
}
